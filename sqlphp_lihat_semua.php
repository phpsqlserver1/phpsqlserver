<?php
$serverName = "akfi-dev01"; //serverName\instanceName
$connectionInfo = array("Database"=>"sqlphp", "UID"=>"sqlphp", "PWD"=>"123456");
$conn = sqlsrv_connect($serverName, $connectionInfo);
 
if( $conn ) {


  
/* Set up and execute the query. */  
$tsql = "SELECT  nama_kapal,dokumen FROM sqlphptbl";  
$stmt = sqlsrv_query( $conn, $tsql);  
if( $stmt === false)  
{  
     echo "Error in query preparation/execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
  
/* Retrieve each row as an associative array and display the results.*/  
while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))  
{  
      echo $row['nama_kapal'].", ".$row['dokumen']."<br>";  
}  
  
/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  

}else{
echo "Connection could not be established.<br />";
die( print_r(sqlsrv_errors(), true));
}

?>